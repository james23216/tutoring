const express = require('express');
const router = express.Router();
const Systems = require('../models/systems');

router.post('/load', (req, res, next) => {
    Systems.loadSystems(req.body, res);
});

router.post('/update', (req, res, next) => {
    Systems.updateSystems(req.body, res);
});

router.post('/remove', (req, res, next) => {
    Systems.removeSystems(req.body, res);
});

router.post('/save', (req, res, next) => {
    Systems.saveSystems(req.body, res);
});

module.exports = router;