-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2018 at 06:01 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cez`
--

-- --------------------------------------------------------

--
-- Table structure for table `templatefile`
--

CREATE TABLE `templatefile` (
  `id` int(11) NOT NULL,
  `AppHost` tinytext NOT NULL,
  `DBHost` tinytext NOT NULL,
  `IFN` char(15) NOT NULL,
  `CFN` char(15) NOT NULL,
  `OSType` set('AIX','RHEL_Linux','Suse_Linux','Windows') NOT NULL,
  `sid_lc` char(3) NOT NULL,
  `sidadm` char(6) NOT NULL,
  `SID_UC` char(3) NOT NULL,
  `DBType` set('db2','ora','syb','mss','hdb','none') NOT NULL,
  `AppType` set('StandardABAPJava','BOBJ','APOwLC','none','ConvergentCharging') NOT NULL,
  `num1` tinyint(4) NOT NULL,
  `num2` tinyint(4) NOT NULL,
  `num3` tinyint(4) NOT NULL,
  `num4` tinyint(4) NOT NULL,
  `num5` tinyint(4) NOT NULL,
  `num6` tinyint(4) NOT NULL,
  `CDIR` char(3) NOT NULL,
  `CustName` char(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `templatefile`
--

INSERT INTO `templatefile` (`id`, `AppHost`, `DBHost`, `IFN`, `CFN`, `OSType`, `sid_lc`, `sidadm`, `SID_UC`, `DBType`, `AppType`, `num1`, `num2`, `num3`, `num4`, `num5`, `num6`, `CDIR`, `CustName`) VALUES
(2, 'atlddir', 'atlddir', '10.177.68.15', '10.140.4.15', 'AIX', 'ddi', 'ddiadm', 'DDI', 'db2', 'StandardABAPJava', 34, 35, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(3, 'atldsmr', 'atldsmr', '10.177.68.46', '10.140.4.46', 'AIX', 'dsm', 'dsmadm', 'DSM', 'db2', 'StandardABAPJava', 40, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(4, 'atldsmr', 'atldsmr', '10.177.68.46', '10.140.4.46', 'AIX', 'dsj', 'dsjadm', 'DSJ', 'db2', 'StandardABAPJava', 1, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(5, 'atldacr', 'atldacr', '10.177.68.51', '10.140.4.51', 'AIX', 'dac', 'dacadm', 'DAC', 'db2', 'StandardABAPJava', 40, 41, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(6, 'atldacr', 'atldacr', '10.177.68.51', '10.140.4.51', 'AIX', 'cac', 'cacadm', 'CAC', 'db2', 'StandardABAPJava', 20, 21, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(7, 'atldscr', 'atldscr', '10.177.68.12', '10.140.4.12', 'AIX', 'dsc', 'dscadm', 'DSC', 'db2', 'StandardABAPJava', 56, 57, 58, 0, 0, 0, 'CEZ', 'Celanese'),
(8, 'atlcemr', 'atlcemr', '10.177.68.28', '10.140.4.28', 'AIX', 'dem', 'demadm', 'DEM', 'db2', 'StandardABAPJava', 6, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(9, 'atlcemr', 'atlcemr', '10.177.68.28', '10.140.4.28', 'AIX', 'cem', 'cemadm', 'CEM', 'db2', 'StandardABAPJava', 7, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(10, 'atlcfgr', 'atlcfgr', '10.177.68.48', '10.140.4.48', 'AIX', 'dfg', 'dfgadm', 'DFG', 'db2', 'StandardABAPJava', 40, 41, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(11, 'atlcfgr', 'atlcfgr', '10.177.68.48', '10.140.4.48', 'AIX', 'cfg', 'cfgadm', 'CFG', 'db2', 'StandardABAPJava', 20, 21, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(12, 'atlxrpr', 'atlxrpr', '10.177.68.50', '10.140.4.50', 'AIX', 'xrp', 'xrpadm', 'XRP', 'db2', 'StandardABAPJava', 41, 40, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(13, 'ralxxir', 'ralxxir', '10.177.68.55', '10.140.4.55', 'AIX', 'xxi', 'xxiadm', 'XXI', 'db2', 'StandardABAPJava', 40, 41, 42, 0, 0, 0, 'CEZ', 'Celanese'),
(14, 'rallegnilerp', 'rallegnilerp', '10.177.68.59', '10.140.4.59', 'AIX', 'zp1', 'zp1adm', 'ZP1', 'db2', 'StandardABAPJava', 50, 51, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(15, 'atlchrr', 'atlchrr', '10.177.68.22', '10.140.4.22', 'AIX', 'dhr', 'dhradm', 'DHR', 'db2', 'StandardABAPJava', 40, 2, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(16, 'atlchrr', 'atlchrr', '10.177.68.22', '10.140.4.22', 'AIX', 'chr', 'chradm', 'CHR', 'db2', 'StandardABAPJava', 44, 1, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(17, 'raldrwr', 'raldrwr', '10.177.68.56', '10.140.4.56', 'AIX', 'drw', 'drwadm', 'DRW', 'db2', 'StandardABAPJava', 40, 41, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(18, 'atlcpor', 'atlcpor', '10.177.68.13', '10.140.4.13', 'AIX', 'dpo', 'dpoadm', 'DPO', 'db2', 'StandardABAPJava', 40, 41, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(19, 'atlcpor', 'atlcpor', '10.177.68.13', '10.140.4.13', 'AIX', 'cpo', 'cpoadm', 'CPO', 'db2', 'StandardABAPJava', 88, 89, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(20, 'atldslr', 'atldslr', '10.177.68.14', '10.140.4.14', 'AIX', 'dsl', 'dsladm', 'DSL', 'db2', 'StandardABAPJava', 62, 63, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(21, 'atlcepr', 'atlcepr', '10.177.68.18', '10.140.4.18', 'AIX', 'dep', 'depadm', 'DEP', 'db2', 'StandardABAPJava', 40, 41, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(22, 'atlcepr', 'atlcepr', '10.177.68.18', '10.140.4.18', 'AIX', 'cep', 'cepadm', 'CEP', 'db2', 'StandardABAPJava', 60, 61, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(23, 'atlcgtr', 'atlcgtr', '10.177.68.19', '10.140.4.19', 'AIX', 'dgt', 'dgtadm', 'DGT', 'db2', 'StandardABAPJava', 40, 42, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(24, 'atlcgtr', 'atlcgtr', '10.177.68.19', '10.140.4.19', 'AIX', 'cgt', 'cgtadm', 'CGT', 'db2', 'StandardABAPJava', 77, 79, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(25, 'atlcrpr', 'atlcrpr', '10.177.68.8', '10.140.4.8', 'AIX', 'drp', 'drpadm', 'DRP', 'db2', 'StandardABAPJava', 41, 40, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(26, 'atlcrpr', 'atlcrpr', '10.177.68.8', '10.140.4.8', 'AIX', 'crp', 'crpadm', 'CRP', 'db2', 'StandardABAPJava', 46, 45, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(27, 'atlcxir', 'atlcxir', '10.177.68.21', '10.140.4.21', 'AIX', 'dxi', 'dxiadm', 'DXI', 'db2', 'StandardABAPJava', 40, 39, 41, 0, 0, 0, 'CEZ', 'Celanese'),
(28, 'atlcxir', 'atlcxir', '10.177.68.21', '10.140.4.21', 'AIX', 'cxi', 'cxiadm', 'CXI', 'db2', 'StandardABAPJava', 36, 35, 37, 0, 0, 0, 'CEZ', 'Celanese'),
(29, 'atlpr3r', 'atlpr3r', '10.177.68.31', '10.140.4.31', 'AIX', 'pr3', 'pr3adm', 'PR3', 'db2', 'StandardABAPJava', 0, 1, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(30, 'atlup11', 'atlpr3r', '10.177.68.32', '10.140.4.32', 'AIX', 'pr3', 'pr3adm', 'PR3', 'db2', 'StandardABAPJava', 2, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(31, 'atlup12', 'atlpr3r', '10.177.68.33', '10.140.4.33', 'AIX', 'pr3', 'pr3adm', 'PR3', 'db2', 'StandardABAPJava', 4, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(32, 'atlpslr', 'atlpslr', '10.177.68.39', '10.140.4.39', 'AIX', 'psl', 'psladm', 'PSL', 'db2', 'StandardABAPJava', 62, 63, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(33, 'atlpsmr', 'atlpsmr', '10.177.68.47', '10.140.4.47', 'AIX', 'psm', 'psmadm', 'PSM', 'db2', 'StandardABAPJava', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(34, 'atlpsmr', 'atlpsmr', '10.177.68.47', '10.140.4.47', 'AIX', 'psj', 'psjadm', 'PSJ', 'db2', 'StandardABAPJava', 3, 1, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(35, 'ralprwr', 'ralprwr', '10.177.68.57', '10.140.4.57', 'AIX', 'prw', 'prwadm', 'PRW', 'db2', 'StandardABAPJava', 0, 1, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(36, 'atlpemr', 'atlpemr', '10.177.68.36', '10.140.4.36', 'AIX', 'pem', 'pemadm', 'PEM', 'db2', 'StandardABAPJava', 40, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(37, 'atlpepr', 'atlpepr', '10.177.68.43', '10.140.4.43', 'AIX', 'pep', 'pepadm', 'PEP', 'db2', 'StandardABAPJava', 60, 61, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(38, 'atlpfgr', 'atlpfgr', '10.177.68.52', '10.140.4.52', 'AIX', 'pfg', 'pfgadm', 'PFG', 'db2', 'StandardABAPJava', 0, 1, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(39, 'atlpxir', 'atlpxir', '10.177.68.41', '10.140.4.41', 'AIX', 'pxi', 'pxiadm', 'PXI', 'db2', 'StandardABAPJava', 36, 35, 37, 0, 0, 0, 'CEZ', 'Celanese'),
(40, 'atlpacr', 'atlpacr', '10.177.68.53', '10.140.4.53', 'AIX', 'pac', 'pacadm', 'PAC', 'db2', 'StandardABAPJava', 0, 1, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(41, 'atlpgtr', 'atlpgtr', '10.177.68.35', '10.140.4.35', 'AIX', 'pgt', 'pgtadm', 'PGT', 'db2', 'StandardABAPJava', 77, 79, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(42, 'atlphrr', 'atlphrr', '10.177.68.42', '10.140.4.42', 'AIX', 'phr', 'phradm', 'PHR', 'db2', 'StandardABAPJava', 44, 1, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(43, 'atlppor', 'atlppor', '10.177.68.30', '10.140.4.30', 'AIX', 'ppo', 'ppoadm', 'PPO', 'db2', 'StandardABAPJava', 88, 89, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(44, 'atlpscr', 'atlpscr', '10.177.68.34', '10.140.4.34', 'AIX', 'psc', 'pscadm', 'PSC', 'db2', 'StandardABAPJava', 56, 57, 58, 0, 0, 0, 'CEZ', 'Celanese'),
(45, 'usbd11cez1007', 'usbd11cez1007', '10.177.39.7', '10.140.15.7', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(46, 'usbd11cez100b', 'usbd11cez100b', '10.177.39.11', '10.140.15.11', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(47, 'usbd11cez1010', 'usbd11cez1010', '10.177.39.16', '10.140.15.16', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(48, 'usbd11cez101d', 'usbd11cez101d', '10.177.39.29', '10.140.15.29', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(49, 'usbd11cez1011', 'usbd11cez1011', '10.177.39.17', '10.140.15.17', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(50, 'usbd11cez1012', 'usbd11cez1012', '10.177.39.18', '10.140.15.18', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(51, 'usbd11cez1014', 'usbd11cez1014', '10.177.39.20', '10.140.15.20', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(52, 'usbd11cez1015', 'usbd11cez1015', '10.177.39.21', '10.140.15.21', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(53, 'usbd11cez1018', 'usbd11cez1018', '10.177.39.24', '10.140.15.24', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(54, 'usbd11cez100c', 'usbd11cez100c', '10.177.39.12', '10.140.15.12', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(55, 'usbd11cez100d', 'usbd11cez100d', '10.177.39.13', '10.140.15.13', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(56, 'usbd11cez100e', 'usbd11cez100e', '10.177.39.14', '10.140.15.14', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(57, 'usbd11cez1013', 'usbd11cez1013', '10.177.39.19', '10.140.15.19', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(58, 'usbd11cez1008', 'usbd11cez1008', '10.177.39.8', '10.140.15.8', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(59, 'usbd11cez1016', 'usbd11cez1016', '10.177.39.22', '10.140.15.22', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese'),
(60, 'usbd11cez1017', 'usbd11cez1017', '10.177.39.23', '10.140.15.23', 'AIX', 'non', 'nonadm', 'NON', 'none', 'none', 0, 0, 0, 0, 0, 0, 'CEZ', 'Celanese');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `templatefile`
--
ALTER TABLE `templatefile`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `templatefile`
--
ALTER TABLE `templatefile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
