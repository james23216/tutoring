import { Routes } from '@angular/router';

import { SystemsComponent }   from './components/systems/systems.component';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'systems',
        pathMatch: 'full',
    },
    {
        path: 'systems',
        component: SystemsComponent
    }
]
