const mysql = require('mysql');
const config = require('../config/database');
const tables = require('../config/tables');

// Connect To Database - AWESOME
const pool = module.exports = mysql.createPool(config);
const tablename = "templatefile";

module.exports.loadSystems = function(param, res) {

    let query = "SELECT * FROM " + tablename;

    if(param.key != '') {
        query = "SELECT * FROM " + tablename + " WHERE ";
        for(i=0; i<tables.systems.length; i++)
        {
            query += tables.systems[i] + " LIKE '%" + param.key + "%'";
            if((tables.systems.length - 1) != i) 
                query += " OR ";
        }
    }
    pool.query(query, (error, results, fields) => {

        res.json({
            result: true,
            data: results
        });
   });
}

module.exports.updateSystems = function(param, res) {
    
    let query = "UPDATE " + tablename + " SET ? WHERE ?"
    
    pool.query(query, [param, {id: param.id}], (error, results, fields) => {
        res.json({
            result: true,
            data: results
        });
    });
}

module.exports.removeSystems = function(param, res) {
    
    let query = "DELETE FROM " + tablename + " WHERE ?"
    
    pool.query(query, [param], (error, results, fields) => {
        res.json({
            result: true,
            data: results
        });
    });
}

module.exports.saveSystems = function(param, res) {
    
    let keys = Object.keys(param);
    console.log(keys);

    let vals = Object.values(param);
    console.log(vals);

    let aparam = [];
    aparam.push(vals);

    let query = "INSERT INTO " + tablename + " (" + keys + ")" +" VALUES ?";
    
    pool.query(query, [aparam], (error, results, fields) => {
        console.log(error);
        res.json({
            result: true,
            data: results
        });
    });
}
